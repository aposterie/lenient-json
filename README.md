# lenient-json

Extended JSON with benefits. Supports `NaN`, `Infinity`, `undefined`, array holes and any type of JavaScript string quotes.

## Usage
```bash
npm install --save lenient-json
```

```javascript
const lson = require('lenient-json');
lson.parse('[,,NaN,]'); // [undefined, undefined, NaN, undefined]
lson.parse("{'a': -Infinity }"); // { a: -Infinity }
```

## License
BSD 3 Clause.