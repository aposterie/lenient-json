# Lenient JSON
# A superset of Standard ECMA-404 data interchange format.
#
# Supporting:
# 1. Array holes
# 2. Three types of string quotes.
# 3. NaN, Infinity, null and undefined

const quotes = <[ " ' ` ]>
const back-slash = '\\'

at = 0      # Index
ch = ''     # Current Character
escapee =
	'"': '"'
	'\\': '\\'
	'/': '/'
	b: '\b'
	f: '\f'
	n: '\n'
	r: '\r'
	t: '\t'

text = ''

error = (message) ->
	throw new SyntaxError message, at, text

/** Assert the current character, and jump to the next one.
 * @param {char} c
 */
next = (c) ->
	if c and c isnt ch
		error "Expected '#{c}' instead of '#{ch}'"

	# Get the next character. When there are no more characters,
	# return the empty string.
	ch := text.charAt at
	at += 1
	return ch

/** Calls next if the current character is the given one.
 * @param {string}
 */
eat = (c) ->
	if ch is c then next c else false

/* Check what the next c characters are, or whether
 * they are the given string.
 * @param {string|number} c
 * @returns {string|boolean}
 */
peak = (c = 1) ->
	if typeof c is 'string'
		c is text.substr ch + 1, c
	else
		text.substr ch + 1, c

/** Multiple next() call
 * @param {string} m
 */
next-word = (w) !->
	[...w].forEach next

/** Parses a numeric value.
 * @returns {number}
 */
number = ->
	value = 0
	string = ''

	if ch is '-'
		string = '-'
		next '-'

	if ch is 'I'
		next-word 'Infinity'
		return -Infinity

	while '0' <= ch <= '9'
		string += ch
		next()

	if ch is '.'
		string += '.'
		while next() and '0' <= ch <= '9'
			string += ch

	if ch is 'e' or ch is 'E'
		string += ch
		next()
		if ch is '-' or ch is '+'
			string += ch
			next()

		while '0' <= ch <= '9'
			string += ch
			next()

	value = +string
	unless isFinite value
		error 'Bad number'
	else
		return value

/** Parses a string.
 * @returns {string}
 */
string = ->
	i = 0
	value = ''
	uffff = 0

	if ch in quotes
		quote-char = ch
		while next()
			if ch is quote-char
				next()
				return value

			if ch is back-slash
				next()
				if ch is 'u'
					uffff = 0
					for i from 0 til 4
						hex = parseInt next(), 16
						break unless isFinite hex
						uffff = uffff * 16 + hex

					value += String.fromCharCode uffff
				else if typeof escapee[ch] is 'string'
					value += escapee[ch]
				else
					break

			else
				value += ch

	error 'Bad string'

/** Consumes all following whitespaces.
 */
white = !->
	# Skip whitespace.
	[ next() while ch and ch <= ' ' ]


word = -> switch ch
	# true, false, or null.
	| 't' => next-word 'true'    ; true
	| 'f' => next-word 'false'   ; false
	| 'n' => next-word 'null'    ; null
	| 'N' => next-word 'NaN'     ; NaN
	| 'I' => next-word 'Infinity'; Infinity
	| _   => error "Unexpected '#{ch}'"

array = ->
	# Parse an array value.
	const arr = []
	if eat '['
		white()
		if eat ']'
			return arr   # empty array

		while ch
			# Array holes
			arr.push if ch in ',]' then void else value()

			white()
			return arr if eat ']'

			next ','
			white()

	error 'Bad array'

object = !->
	# Parse an object value.
	obj = Object.create null

	if eat '{'
		white()
		return obj if eat '}'

		while ch
			key = string()
			white()
			next ':'
			obj[key] = value()
			white()
			return obj if eat '}'

			next ','
			white()

	error 'Bad object'

value = ->
	# Parse a JSON value. It could be an object, an array, a string, a number,
	# or a word.
	white()
	switch ch
	| \{        => object()
	| \[        => array()
	| \" \" \`  => string()
	| \-        => number()
	| otherwise => if '0' <= ch <= '9' then number() else word()

# Following the JSON API.
module.exports = (source) ->
	unless typeof source is 'string'
		throw TypeError 'JSON.parse cannot parse non-strings.'

	text := source
	at := 0
	ch := ' '
	result = value()
	white()
	if ch
		error 'Syntax error'

	return result